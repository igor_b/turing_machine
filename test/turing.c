#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#ifndef bool
#define bool int
#endif
#ifndef true
#define true 1
#endif
#ifndef false
#define false 0
#endif

#define TERM_COLOR_RED      "\x1b[31m"
#define TERM_COLOR_RED_INV  "\x1b[41m"
#define TERM_COLOR_GREEN    "\x1b[32m"
#define TERM_COLOR_YELLOW   "\x1b[33m"
#define TERM_COLOR_BLUE     "\x1b[34m"
#define TERM_COLOR_PINK     "\x1b[35m"
#define TERM_COLOR_PURPLE   "\x1b[95m"
#define TERM_COLOR_CYAN     "\x1b[36m"
#define TERM_COLOR_WHITE    "\x1b[37m"
#define TERM_COLOR_ORANGE   "\x1b[91m"
#define TERM_COLOR_INVERT   "\x1b[7m"
#define TERM_COLOR_RESET    "\x1b[0m"


typedef enum{ST_NONE, ST_A, ST_B, ST_C, ST_D, ST_E, ST_F, ST_G, ST_H, ST_I, ST_J, ST_K, ST_L, ST_M, ST_N, ST_HALT} state_t;
const char* states_n[] = {"NONE", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "HALT"};

typedef struct{
    state_t state;
    char symbol;
    int32_t move;
    char write;
    state_t next_state;
} rule_t;

bool b = 1;

//#define ALG_BEAVER3
#define ALG_BEAVER4
//#define ALG_ANT


#define TAPE_LENGTH 10000
#define FIELD_WIDTH TAPE_LENGTH
char tape[TAPE_LENGTH];
uint32_t tape_idx = TAPE_LENGTH/2;
uint32_t tape_min_max[2] = {TAPE_LENGTH/2, TAPE_LENGTH/2};

state_t state=ST_B;


#ifdef ALG_ANT

#undef FIELD_WIDTH
//#define FIELD_WIDTH (TAPE_LENGTH/40)
#define FIELD_WIDTH 180
#define STOP_AFTER 10800

rule_t rules[] = {

    {.symbol='1', .state=ST_A, .write='B', .move=FIELD_WIDTH,  .next_state=ST_D},
    {.symbol='B', .state=ST_A, .write='1', .move=-FIELD_WIDTH,  .next_state=ST_B},

    {.symbol='1', .state=ST_B, .write='B', .move=1,  .next_state=ST_A},
    {.symbol='B', .state=ST_B, .write='1', .move=-1,  .next_state=ST_C},

    {.symbol='1', .state=ST_C, .write='B', .move=-FIELD_WIDTH,  .next_state=ST_B},
    {.symbol='B', .state=ST_C, .write='1', .move=FIELD_WIDTH,  .next_state=ST_D},

    {.symbol='1', .state=ST_D, .write='B', .move=-1,  .next_state=ST_C},
    {.symbol='B', .state=ST_D, .write='1', .move=1,  .next_state=ST_A},

    {.state=ST_NONE}
};




#endif

#ifdef ALG_BEAVER3
// 3-state busy beaver
rule_t rules[] = {
    {.symbol='0', .state=ST_A, .write='1', .move=1,  .next_state=ST_B},
    {.symbol='1', .state=ST_A, .write='1', .move=0,  .next_state=ST_HALT},
    {.symbol='B', .state=ST_A, .write='1', .move=1,  .next_state=ST_B},

    {.symbol='0', .state=ST_B, .write='0', .move=1,  .next_state=ST_C},
    {.symbol='1', .state=ST_B, .write='1', .move=1,  .next_state=ST_B},
    {.symbol='B', .state=ST_B, .write='0', .move=1,  .next_state=ST_C},

    {.symbol='0', .state=ST_C, .write='1', .move=-1,  .next_state=ST_C},
    {.symbol='1', .state=ST_C, .write='1', .move=-1,  .next_state=ST_A},
    {.symbol='B', .state=ST_C, .write='1', .move=-1,  .next_state=ST_C},


    {.state=ST_NONE}
};
#endif


#ifdef ALG_BEAVER4
// 4-state busy beaver
rule_t rules[] = {
    {.symbol='0', .state=ST_A, .write='1', .move=1, .next_state=ST_B},
    {.symbol='1', .state=ST_A, .write='1', .move=-1, .next_state=ST_B},
    {.symbol='B', .state=ST_A, .write='1', .move=1, .next_state=ST_B},

    {.symbol='0', .state=ST_B, .write='1', .move=-1, .next_state=ST_A},
    {.symbol='1', .state=ST_B, .write='0', .move=-1, .next_state=ST_C},
    {.symbol='B', .state=ST_B, .write='1', .move=-1, .next_state=ST_A},

    {.symbol='0', .state=ST_C, .write='1', .move=0, .next_state=ST_HALT},
    {.symbol='1', .state=ST_C, .write='1', .move=-1, .next_state=ST_D},
    {.symbol='B', .state=ST_C, .write='1', .move=0, .next_state=ST_HALT},

    {.symbol='0', .state=ST_D, .write='1', .move=1, .next_state=ST_D},
    {.symbol='1', .state=ST_D, .write='0', .move=1, .next_state=ST_A},
    {.symbol='B', .state=ST_D, .write='1', .move=1, .next_state=ST_D},

    {.state=ST_NONE}
};
#endif


void print_state(void){
    printf("current_state=%s, tape_idx=%d\n", states_n[state], tape_idx);
    printf("\033[2J");
    printf("\033[%dA", TAPE_LENGTH/FIELD_WIDTH+1);
    for(int i=0;i<TAPE_LENGTH;i++){
    //for(int i=tape_min_max[0];i<=tape_min_max[1];i++){
        if(tape_idx==i) printf("%s", TERM_COLOR_INVERT);
        if(tape[i]=='1') printf("%s1", TERM_COLOR_GREEN);
        else if(tape[i]=='0') printf("%s0", TERM_COLOR_YELLOW);
        else printf(".");
        printf("%s", TERM_COLOR_RESET);
        if((i+1)>=FIELD_WIDTH && (i+1)%FIELD_WIDTH==0) printf("\n");
    }
    printf("\n");
    usleep(5000);
}


void print_rule(int idx){
    const rule_t *r = &rules[idx];
    printf("(st:%s, sym:%c, wr:%c, mv:%2d, nxt:%s)\n", states_n[r->state], r->symbol, r->write, r->move, states_n[r->next_state]);
};


int main(){
    uint32_t step_count = 0;

    // clear the tape
    for(int i=0;i<TAPE_LENGTH;i++) tape[i]='B';

    // engage the machine
    while(1){
        int ridx=0;
        uint8_t rule_found = 0;
        step_count++;
        while(rules[ridx].state!=ST_NONE){
            rule_t *r = &rules[ridx];
            if(r->state==state && tape[tape_idx]==r->symbol){
    //            print_rule(ridx);
                tape[tape_idx] = r->write;
                tape_idx += r->move;
                state = r->next_state;

                // housekeeping
                if(tape_idx<tape_min_max[0]) tape_min_max[0] = tape_idx;
                else if(tape_idx>tape_min_max[1]) tape_min_max[1] = tape_idx;
                if(tape_idx>TAPE_LENGTH-1){ // unsigned idx will wrap to high values
                    printf("Out of memory!\n");
                    state = ST_HALT;
                }

                rule_found = 1;
                break;
            }
            ridx++;
        }
    print_state();
        if(!rule_found){
            printf("Exception: rule not found!\n");
            break;
        }

        if(state==ST_HALT){
            printf("HALT\n");
            break;
        }

#ifdef STOP_AFTER
        if(step_count>STOP_AFTER){
            printf("ERR: too many steps.\n");
            break;
        }
#endif
    }

    print_state();
    printf("step count: %d\n", step_count);

    return 0;
}

