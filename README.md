# Turing machine

This repository contains files and code for simple hardware Turing machine simulator.

More info: [http://hyperglitch.com/articles/turing-machine](http://hyperglitch.com/articles/turing-machine)

Video: [https://www.youtube.com/watch?v=ivPv_kaYuwk](https://www.youtube.com/watch?v=ivPv_kaYuwk)


## Short description

The machine consists of two systems: tape and the reading/writing head. Both systems share the same GND (for simpler and more reliable tape cell toggling).

Tape is made of a RGB LED strip where each LED has one exposed pad in front of it used for toggling its state (blank/0/1).

Read/write head uses two LDRs with color filters to check the symbol on tape (LED color). Lever connected to servo is used to toggle the state of the tape cell. Rules are given by using punched cards.


## Directory structure

* `3d` - 3D models of machine
* `card_reader` - Kicad project for card reader
* `tape_pcb` - Kicad project for tape input
* `turing_machine` - main Arduino code
* `turing_tape` - Arduino code for the "tape"


## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Copyright: Igor Brkic, 2016