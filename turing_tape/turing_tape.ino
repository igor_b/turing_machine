

#include <FastLED.h>
#define NUM_LEDS 56
#define DATA_PIN_TAPE 12
#define DATA_PIN_MARKER 10

CRGB leds[2][NUM_LEDS];



#define NUMBER_OF_SHIFT_CHIPS   7
#define DATA_WIDTH   NUMBER_OF_SHIFT_CHIPS * 8
#define PULSE_WIDTH_USEC   5
#define DEBOUNCE_INTERVAL 80

uint32_t inputs_time[DATA_WIDTH] = {0};
uint8_t inputs_old[DATA_WIDTH] = {1};
uint8_t inputs_temp[DATA_WIDTH] = {1};
uint8_t pins[DATA_WIDTH] = {1};

const int ploadPin = 5;
const int dataPin  = 3;
const int clockPin = 4;

const int clearPin = 2;

// set led color
void key_pressed(int idx){
  Serial.print("key pressed: ");
  Serial.println(idx);
  if(idx>=NUM_LEDS) return;
  if(leds[0][idx].r>0) leds[0][idx] = CRGB::Green;
  else if(leds[0][idx].g>0) leds[0][idx] = CRGB::Black;
  else leds[0][idx] = CRGB::Red;
}


void read_shift_regs(){
  uint8_t b;
  const uint32_t tm = millis();
  bool change = false;
  // load values
  digitalWrite(ploadPin, LOW);
  delayMicroseconds(PULSE_WIDTH_USEC);
  digitalWrite(ploadPin, HIGH);

  // read em up
  for(int i = 0; i < DATA_WIDTH; i++){
    b = digitalRead(dataPin);
    const int idx = i;
    if(b != inputs_temp[idx]){
      inputs_time[idx] = tm;
      inputs_temp[idx] = b;
    }
    if(tm-inputs_time[idx] > DEBOUNCE_INTERVAL){
      if(b!=inputs_old[idx]){
        pins[idx] = b;
        inputs_old[idx]=b;
        if(b==0){
          // activation
          key_pressed(i);
          change = true;
        }
      }
    }

    digitalWrite(clockPin, HIGH);
    delayMicroseconds(PULSE_WIDTH_USEC);
    digitalWrite(clockPin, LOW);
  }

  if(change){
    FastLED.show();
  }
}


void display_pin_values(){
  for(int i = 0; i < DATA_WIDTH; i++){
    Serial.print(pins[i]);
  }
  Serial.println();
}

static bool clear_leds = false;
void clearLEDs(){
  clear_leds = true;
}

void setup(){
  Serial.begin(115200);

  FastLED.addLeds<NEOPIXEL, DATA_PIN_TAPE>(leds[0], NUM_LEDS);
  FastLED.addLeds<NEOPIXEL, DATA_PIN_MARKER>(leds[1], NUM_LEDS);
  FastLED.setBrightness(50);
  FastLED.show();

  pinMode(ploadPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, INPUT);

  digitalWrite(clockPin, LOW);
  digitalWrite(ploadPin, HIGH);

  for(int i=0;i<DATA_WIDTH;i++){
    pins[i] = 1;
    inputs_old[i] = 1;
    inputs_temp[i] = 1;
    inputs_time[i] = 0;
  }

  pinMode(clearPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(clearPin), clearLEDs, FALLING);  

  for(int i=0;i<NUM_LEDS;i++) leds[0][i]=CRGB::Black;
  for(int i=0;i<NUM_LEDS;i++) leds[1][i]=CRGB::Blue;
  FastLED.show();
  
}

void loop(){
  read_shift_regs();
  //display_pin_values();

  delay(10);

  if(clear_leds){
    clear_leds=false;
    for(int i=0;i<NUM_LEDS;i++) leds[0][i]=CRGB::Black;
    FastLED.show();
  }

}
