#include <Servo.h>
#include <RunningMedian.h>

#include <FastLED.h>

// ---------------------------------------------------
// setup pins
// ---------------------------------------------------

// card status LEDS
const int latchPin = 5;
const int clockPin = 6;
const int dataPin  = 7;

// card servo
// stop: 104
// CCW: 107-113
// CW: 100-90
Servo servo_card;
const int servo_card_pin = 8;

// card LDR muxing
const int mux[3]={2, 3, 4};
const int mux_out[2] = {A0, A1};

// drive train
const int motor[] = {12, 11}; 

// position
const int marker = A3;

// color detection
const int color[2] = {A4, A5};

// head servos
const int head_read_pin = 9;
const int head_write_pin = 10;
Servo head_read;
Servo head_write;
const int head_read_limits[] = {10, 50};
const int head_write_limits[]= {180, 140};

#define head_read_set(a) constrain(map((a), 0, 9, head_read_limits[0], head_read_limits[1]), head_read_limits[0], head_read_limits[1])
#define head_write_set(a) constrain(map((a), 0, 9, head_write_limits[0], head_write_limits[1]), head_write_limits[1], head_write_limits[0])

// machine state led strip
#define state_num_leds 8
const int state_leds_pin = 13;
CRGB state_leds[state_num_leds];

// halt button
const int halt = A2;


// ----------------------------------------------
// card reader stuff
// ----------------------------------------------
RunningMedian ldr_samples = RunningMedian(5);

const int ldr_thr[] = {
  40, 70, 25, 15, 100, 70,
  30, 120, 90, 40, 60, 50
};

const int leds[12]   = {1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13};
const int ldrs[12]   = {0, 1, 2, 5, 7, 6, 4, 3,  2,  1,  0,  3};
const int ldr_mux[12]= {0, 0, 0, 0, 0, 0, 0, 0,  1,  1,  1,  1};
bool state[12] = {false};

uint16_t gui_leds=0;
void update_gui(){
  digitalWrite(latchPin, LOW);
  // shift out the bits:
  shiftOut(dataPin, clockPin, MSBFIRST, (uint8_t)(gui_leds>>8));  
  shiftOut(dataPin, clockPin, MSBFIRST, (uint8_t)(gui_leds&0xff));  

  //take the latch pin high so the LEDs will light up:
  digitalWrite(latchPin, HIGH);
}

void set_one(int idx, int brightness){
  if(brightness>0){
    gui_leds |= 0x01L<<leds[idx];
  }
  else{
    gui_leds &= ~(0x01<<leds[idx]);
  } 
}


#define CARD_START 0x18D
#define CARD_EMPTY 0xfff
#define CARD_NONE  0x00


#define CARD_WAIT 2000
#define CARD_DEBOUNCE 300
#define CARD_TIMEOUT 5000

#define READ_HEAD_DOWN() head_read.write(head_read_set(2))
#define READ_HEAD_UP() head_read.write(head_read_set(7))

enum{SYM_BLANK=0, SYM_ZERO, SYM_ONE, SYM_HALT};

enum {FWD, BCK, HALT};
#define rev(c) ((c)==FWD?BCK:FWD)

void setup() {
  Serial.begin(115200);

  // card reader
  pinMode(mux[0], OUTPUT);
  pinMode(mux[1], OUTPUT);
  pinMode(mux[2], OUTPUT);
  
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  
  digitalWrite(mux[0], 0);
  digitalWrite(mux[1], 0);
  digitalWrite(mux[2], 0);

  //servo_card.attach(8);

  // drive train
  pinMode(motor[0], OUTPUT);
  pinMode(motor[1], OUTPUT);
  digitalWrite(motor[0], LOW);
  digitalWrite(motor[1], LOW);


  // position
  pinMode(marker, INPUT);

  // color detection
  pinMode(color[0], INPUT);
  pinMode(color[1], INPUT);

  // machine state led strip
  FastLED.addLeds<NEOPIXEL, state_leds_pin>(state_leds, state_num_leds);
  FastLED.setBrightness(50);
  FastLED.show();

  // halt button
  pinMode(halt, INPUT_PULLUP);

  update_gui();
  
  // eject card
  const uint8_t c = get_card();
  if(c!=CARD_EMPTY){
    move_card(FWD);
    while(get_card()!=CARD_EMPTY);
    delay(500);
    move_card(HALT);
  }


  // heads up
  head_read.attach(head_read_pin);
  head_write.attach(head_write_pin);
  head_write.write(head_write_set(8));
  head_read.write(head_read_set(7));
  delay(500);
  head_read.detach();
  head_write.detach();
  
}

struct rule_t{
  uint8_t state;
  uint8_t sym;
  uint8_t nstate;
  uint8_t wsym;
  uint8_t mv;
};


uint16_t get_card(){
  uint16_t out = 0;
  for(int i=0;i<12;i++){
    digitalWrite(mux[0], ldrs[i]&0x01);
    digitalWrite(mux[1], (ldrs[i]&0x02)>>1);
    digitalWrite(mux[2], (ldrs[i]&0x04)>>2);
    delay(1);
    for(int j=0;j<5;j++){
      ldr_samples.add(analogRead(mux_out[ldr_mux[i]]));
    }
    out |= (ldr_samples.getMedian()>ldr_thr[i]?1:0)<<i;
    set_one(11-i, ldr_samples.getMedian()>ldr_thr[i]?1:0);
  }
  return out;
}

struct rule_t rule(uint16_t c){
  struct rule_t out;
  out.state = c>>9;
  out.sym = (c>>7)&0x3;
  out.nstate = (c>>4)&0x7;
  out.wsym = (c>>2)&0x3;
  out.mv = (c>>1)&0x1;
  return out;
}

void print_rule(struct rule_t r){
  Serial.print("\state:");
  Serial.print(r.state);
  Serial.print(" sym:");
  Serial.print(r.sym);
  Serial.print(" nstate:");
  Serial.print(r.nstate);
  Serial.print(" write:");
  Serial.print(r.wsym);
  Serial.print(" move:");
  Serial.println(r.mv);
 
}

void move_card(int dir){
  if(dir==HALT){
    servo_card.detach();
    return;
  }
  if(!servo_card.attached()) servo_card.attach(servo_card_pin);
  if(dir==FWD) servo_card.write(75);
  else if(dir==BCK) servo_card.write(42);
}

uint16_t read_rule(int dir){
  uint16_t accum = 0;
  bool got_something = false;
  move_card(dir);
  uint32_t tm = millis();
  while(true){
    const uint16_t c = get_card();
    if(c!=0) got_something=true;
    accum|=c;
    if(c==0 && got_something){
      delay(50);
      move_card(HALT);
      for(int i=0;i<12;i++) set_one(11-i, accum&(1<<i)?1:0);
      update_gui();
      return accum;
    }
    if(millis()-tm>CARD_TIMEOUT){
      move_card(HALT);
      break; // FIXME
    }
  }
  return 0xffff;
}

uint16_t card_old = 0;
uint32_t card_old_tm = 0;

void motor_move(int dir){
  if(dir==FWD){
    analogWrite(motor[1], 155);
    digitalWrite(motor[0], HIGH);
  }
  else if(dir==BCK){
    analogWrite(motor[1], 100);
    digitalWrite(motor[0], LOW);
  }
  else{
    digitalWrite(motor[0], LOW);
    digitalWrite(motor[1], LOW);
  }
}

void move_one_step(int dir){
  motor_move(dir);
  while(analogRead(marker)<300);
  while(analogRead(marker)>80);

  // go a little bit back
  motor_move(rev(dir));
  delay(40);
  motor_move(HALT);
}

void pause(){
  int old=LOW;
  while(true){
    const int n=digitalRead(halt);
    if(n!=old && n==LOW) return;
    old=n;
  }
}


uint8_t read_symbol(){
  const int avg=5;
  int c1=0;
  int c2=0;
  uint8_t out;

  for(int i=0;i<avg;i++){
    c1 += analogRead(color[0]);
    c2 += analogRead(color[1]);
  }
  c1/=avg;
  c2/=avg;
  Serial.print("read: ");
  Serial.print(c1);
  Serial.print("\t");
  Serial.print(c2);
  Serial.print("\t");
  if(c1<400 && c2<400) out=SYM_BLANK;
  else if(c1<c2) out=SYM_ZERO;
  else if(c1>c2) out=SYM_ONE;
  //else if(c1>550 && c2>550) out=SYM_ZERO;
  //else if(c1>550 && c2<550) out=SYM_ONE;
  switch(out){
    case SYM_BLANK:
      Serial.println("#");
      break;
    case SYM_ZERO:
      Serial.println("0");
      break;
    case SYM_ONE:
      Serial.println("1");
  }
  return out;
}

static uint8_t card_dir=FWD;
struct rule_t find_rule(uint8_t sym, uint8_t state){
  struct rule_t r;
  int dir_cnt = 0;
  while(true){
    const uint16_t c = read_rule(card_dir);
    r = rule(c);
    if(c==CARD_START){
      card_dir=rev(card_dir);
      dir_cnt++;
      if(dir_cnt>5) break;
      read_rule(card_dir);
    }
    if(r.state==state && r.sym==sym) return r;
  }

  Serial.println("CARD PANIC!");
  while(true);
}


void write_symbol(uint8_t sym){
  uint8_t mv_dir=BCK;
  int mv_cnt=4;
  while(mv_cnt<30){
    const uint8_t s = read_symbol();
    if(s==sym) return;

    Serial.print("Writing symbol ");
    Serial.println(sym);
    // change state
    head_write.write(head_write_set(0));
    delay(400);
    head_write.write(head_write_set(8));
    delay(600);

    if(read_symbol()==s){
      // head stuck - wiggle it
      Serial.println("...another try");
      for(int i=0;i<mv_cnt/2;i++){
        delay(300);
        motor_move(mv_dir);
        delay(50);
        motor_move(HALT);
      }
      mv_dir = rev(mv_dir);
      mv_cnt += 1;
    }
  }
  Serial.println("PANIC!");
  while(1);
}


enum{ST_INIT, ST_CARD_IN, ST_READY, ST_HALT};
int phase=ST_INIT;
uint8_t current_state = 0;

void loop(){

  // set current state
  for(int i=0;i<state_num_leds;i++){
    state_leds[i]=i==current_state?CRGB(0,100,190):CRGB::Black;
  }
  FastLED.show();

  if(phase==ST_INIT){
    const uint16_t c = get_card();
    update_gui();
    
    if(c==0x00){
      // fast forward to the beggining
      while(read_rule(FWD)!=CARD_START);
      // position head to the first available cell
      move_one_step(FWD);
      phase = ST_READY;
    
      // attach servos
      head_read.attach(head_read_pin);
      head_write.attach(head_write_pin);
      head_write.write(head_write_set(8));
      head_read.write(head_read_set(8));
      delay(500);
    }
  }
  else if(phase==ST_READY){ // do one work cycle
    // read symbol
    READ_HEAD_DOWN();
    delay(300);
    struct rule_t r = find_rule(read_symbol(), current_state);
    print_rule(r);
    if(r.wsym==SYM_HALT){
      phase = ST_HALT; 
    }
    else{
      write_symbol(r.wsym);
    }
    current_state = r.nstate;
    READ_HEAD_UP();
    delay(300);
    move_one_step(r.mv?FWD:BCK);
  }
  else if(phase==ST_HALT){
    while(1); // FIXME
  }



/* OK
  head_write.write(head_write_set(8));

  while(true){
    const int c1=analogRead(color[0]);
    const int c2=analogRead(color[1]);
    Serial.print(c1);
    Serial.print("\t");
    Serial.print(c2);
    Serial.print("\t");
    if(c1<400 && c2<400) Serial.println("BLANK");
    else if(c1>550 && c2>550) Serial.println("RED");
    else if(c1>550 && c2<550) Serial.println("GREEN");
    delay(50);
  }
*/  

/* TODO: check all cells
  for(int i=0;i<15;i++){
    move_one_step(FWD);
    head_write.write(head_write_set(0));
    delay(500);
    head_write.write(head_write_set(8));
    delay(1000);  
    if(!digitalRead(halt)) pause();
  }
    
  for(int i=0;i<15;i++){
    move_one_step(BCK);
    head_write.write(head_write_set(0));
    delay(500);
    head_write.write(head_write_set(8));
    delay(1000);  
    if(!digitalRead(halt)) pause();
  }
*/  


/*
  uint32_t tm = millis();

  uint16_t c = Serial.println(read_first(ddd));
  update_gui();
  if(c==65535) ddd=rev(ddd);
  delay(2000);
*/

/* OK
  Serial.print(digitalRead(marker));
  Serial.print(" ");
  Serial.print(digitalRead(halt));
  Serial.println();
  delay(20);
*/

/* OK
  Serial.print(analogRead(color[0]));
  Serial.print(" ");
  Serial.print(analogRead(color[1]));
  Serial.println();
  delay(20);
*/

/*
  if(Serial.available()){
    int a=Serial.read();
    if(a=='f'){
      analogWrite(motor[1], 165);
      digitalWrite(motor[0], HIGH);
    }
    else if(a=='b'){
      analogWrite(motor[1], 90);
      digitalWrite(motor[0], LOW);
    }
    else{
      digitalWrite(motor[0], LOW);
      digitalWrite(motor[1], LOW);
    }
  }
*/

  /*
  if(Serial.available()){
    int a=Serial.read();
    if(a>='0' && a<='9'){
      const int c = (a-'0');
      head_write.write(head_write_set(c));
      head_read.write(head_read_set(c));
      head_write.attach(head_write_pin);
      head_read.attach(head_read_pin);
      Serial.println(c);
    }
    else{
      head_write.detach();
    }
  }
  */


}
